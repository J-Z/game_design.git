#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

#define high 15
#define width 20

int ball_x,ball_y;
int ball_vx,ball_vy;
int position_x,position_y;
int ridus;
int left,right; 
int canvas[high][width]={0};

void gotoxy(int x,int y)
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos);
}

void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
}

void startup()
{
	ridus=5;
	position_x=high-1;
	position_y=width/2;
	left=position_y-ridus;
	right=position_y+ridus;
	
	ball_x=position_x-1;
	ball_y=position_y;
	ball_vx=-1;
	ball_vy=1;
	canvas[ball_x][ball_y]=1;
	
	int k,i;
	for(k=left;k<=right;k++)
	{
		canvas[position_x][k]=2;
	}
	for(k=0;k<width;k++)
	{
		for(i=0;i<high/4;i++)
			canvas[i][k]=3;
	}
}

void show()
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if(canvas[i][j]==0)
				printf(" ");
			else if(canvas[i][j]==1)
				printf("0");
			else if(canvas[i][j]==2)
				printf("*");
			else if(canvas[i][j]==3)
				printf("#");
		}
		printf("|\n");	
	}
	for(j=0;j<width;j++)
		printf("-");
	printf("\n");
}

void updatawithoutinput()
{
	if(ball_x==high-2)
	{
		if((ball_y>=left)&&(ball_y<=right))
		{
			
		}
		else
		{
			printf("??\n");
			system("pause");
			exit(0);
		}
	}
	
	static int speed=0;
	if(speed<7)
		speed++;
	if(speed==7)
	{
		speed=0;
		
		canvas[ball_x][ball_y]=0;
		
		ball_x=ball_x+ball_vx;
		ball_y=ball_y+ball_vy;
		canvas[ball_x][ball_y]=1;
		
		if((ball_x==0)||(ball_y==high-2))
		ball_vx=-ball_vx;
		if((ball_y==0)||(ball_y==width-1))
		ball_vy=-ball_vy;
		
		if(canvas[ball_x-1][ball_y]==3)
		{
			ball_vx=-ball_vx;
			canvas[ball_x-1][ball_y]=0;
			printf("\a");
		}
	}	
}

void updatawithinput()
{
	char input;
	if(kbhit())
	{
		input=getch();
		if(input=='a'&&left>0)
		{
			canvas[position_x][right]=0;
			position_y--;
			left=position_y-ridus;
			right=position_y+ridus;
			canvas[position_x][left]=2;
		}
		if(input=='d'&&right<width-1)
		{
			canvas[position_x][left]=0;
			position_y++;
			left=position_y-ridus;
			right=position_y+ridus;
			canvas[position_x][right]=2;
		}
	}
}

int main()
{
	startup();
	while(1)
	{
		show();
		HideCursor();
		updatawithoutinput();
		updatawithinput();
	}
	return 0;
 } 
