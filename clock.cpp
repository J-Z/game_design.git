#include<graphics.h>
#include<conio.h>
#include<math.h>
#include<dos.h> 
int main()
{
	initgraph(width, high);
	int center_x, center_y;
	center_x = width\2;
	center_y = high / 2;
	int secondLength = width / 7;
	int minuteLength = width / 6;
	int hourLength = width / 5;

	int secondEnd_x, secondEnd_y;
	int minuteEnd_x, minuteEnd_y;
	int hourEnd_x, hourEnd_y;
	float secondAngle;
	float minuteAngle;
	float hourAngle;

	SYSTEMTIME ti;
	
	BeginBatchDraw();
	while (1)
	{
		setlinestyle(PS_SOLID, 1);
		setcolor(WHITE);
		circle(center_x, center_y, width / 4);

		int x, y, i;
		for (i = 0; i < 60; i++)
		{
			x = center_x + int(width / 4.3*sin(PI * 2 * i / 60));
			y = center_y + int(width / 4.3*cos(PI * 2 * i / 60));

			if (i % 15 == 0)
			{
				bar(x - 5, y - 5, x + 5, y + 5);
			else if (i % 5 == 0)
				circle(x, y, 3);
			else
				putpixel(x, y, WHITE);
			}

			outtextxy(center_x - 25, center_y + width / 6, "??");
			GetLocalTime(&ti);

			secondAngle = ti.wSecond * 2 * PI / 60;
			minuteAngle = ti.wMinute * 2 * PI / 60 + secondAngle / 60;
			hourAngle = ti.wHour * 2 * PI / 12 + minuteAngle / 12;
			secondEnd_x = center_x + secomdLength + sin(secondAngle);
			secondEnd_y = center_y - secomdLength + sin(secondAngle);
			minuteEnd_x = center_x + secondLength * sin(minuteAngle);
			minuteEnd_y = center_y - secondLength * cos(minuteAngle);

			hourEnd_x = center_x + hourLength * sin(hourAngle);
			hourEnd_y = center_y - hourLength * cos(hourAngle);
			ourEnd_y = center_y - hourLength * cos(hourAngle);
			setlinestyle(PS_SOLID, 2);
			setcolor(YELLOW);
			line(center_x, center_y, secondEnd_x, secondEnd_y);
			setlinestyle(PS_SOLID, 4);
			setcolor(BLUE);
			line(center_x, center_y, minuteEnd_x, minuteEnd_y);

			setlinestyle(PS_SOLID, 6);
			setcolor(RED);
			line(center_x, center_y, hourEnd_x, hourEnd_y);

			FlushBatchDraw();
			sleep(10);

			setcolor(BLACK);
			setlinestyle(PS_SOLID, 2);
			line(center_x, center_y, secondEnd_x, secondEnd_y);
			setlinestyle(PS_SOLID, 4);
			line(center_x, center_y, minuteEnd_x, minuteEnd_y);
			setlinestyle(PS_SOLID, 6);
			line(center_x, center_y, hourEnd_x, hourEnd_y);
		}
	}
	EndBatchDraw();
	_getch();
	closegraph();
	return 0;
}
